set -e
docker-compose up -d

echo 'test: go run main.go -- matcher'
pushd ../server
go install
~/go/bin/server -dgraph="localhost:9080" -redis="localhost:6379"
popd
