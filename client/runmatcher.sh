set -e
docker-compose up -d

echo 'test: go run main.go -- matcher'
pushd ../matcher
go install
~/go/bin/matcher -redis="localhost:6379"
popd
