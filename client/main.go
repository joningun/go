/*
 *
 * Copyright 2018 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// The client demonstrates how to supply an OAuth2 token for every RPC.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"time"

	"google.golang.org/grpc/metadata"

	"mdfinder.com/matcher/mp3"
	"mdfinder.com/server/sp3"

	"google.golang.org/grpc"
)

//go:generate goderive .
func main() {
	flag.Parse()
	which := flag.Arg(0)
	log.Printf("arg: %s is given", which)
	if which == "server" {

		testServer()
	} else if which == "matcher" {
		testMatcher()
	}
}
func testMatcher() {
	opts := []grpc.DialOption{
		grpc.WithInsecure(),
	}
	conn, err := grpc.Dial(":50030", opts...)
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := mp3.NewMatchingClient(conn)
	ctx := metadata.NewOutgoingContext(context.Background(), metadata.MD{
		"token": []string{"fuck"},
	})

	newS := func(id string, feats ...*mp3.Feature) *mp3.Sample {
		return &mp3.Sample{
			Id:       id,
			Features: feats,
		}
	}
	newF := func(tag string, plus int64) *mp3.Feature {
		return &mp3.Feature{
			Key:  tag,
			Plus: plus,
		}
	}
	const letterBytes = "ABCDEFGHIJKL"

	rand.Seed(time.Now().Unix())
	randomLetter := func() string {
		b := make([]byte, 1)
		for i := range b {
			b[i] = letterBytes[rand.Intn(len(letterBytes))]
		}
		return string(b)
	}
	randomNum := func() int64 {
		return rand.Int63n(10) + 1
	}
	random3F := func() []*mp3.Feature {
		return []*mp3.Feature{
			newF(randomLetter(), randomNum()), newF(randomLetter(), randomNum()), newF(randomLetter(), randomNum()),
		}
	}
	sampleIDCnt := 0
	randomSam := func() *mp3.Sample {
		sampleIDCnt++
		return newS(fmt.Sprintf("S%d", sampleIDCnt), random3F()...)
	}

	logGroups := func(goups *mp3.Groups) {
		for idx, groupInfo := range goups.Groups {
			log.Printf("Group %d.", idx)
			for _, sampleInfo := range groupInfo.Samples {
				log.Printf("  %s, %v", sampleInfo.Id, sampleInfo.Features)
			}
		}
	}

	queueRandom20Samples := func() {
		for i := 0; i < 20; i++ {
			_, e := c.QueueForMatch(ctx, randomSam())
			if e != nil {
				panic(e)
			}
		}
	}

	getPagedGroups := func(offset, limit int) *mp3.Groups {
		groups, err := c.PagedGroups(ctx, &mp3.PageInfo{
			Offset: 0,
			Limit:  30,
		})
		if err != nil {
			panic(err)
		}
		return groups
	}
	log.Println("Round 1")
	queueRandom20Samples()
	log.Println("queuing finished.")
	time.Sleep(5 * time.Second)
	logGroups(getPagedGroups(0, 20))

	log.Println("Round 2")
	queueRandom20Samples()
	log.Println("queuing finished.")
	time.Sleep(5 * time.Second)
	logGroups(getPagedGroups(0, 20))

	hottags, e := c.HotTags(ctx, &mp3.VoidMsg{})
	if e != nil {
		panic(e)
	}
	log.Println("hot tags:")
	for _, hottag := range hottags.Tags {
		log.Printf("%s: %d", hottag.Tag, hottag.SampleCnt)
	}
}

func testServer() {
	opts := []grpc.DialOption{
		grpc.WithInsecure(),
	}
	conn, err := grpc.Dial(":50030", opts...)
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := sp3.NewServerClient(conn)

	ctx := metadata.NewOutgoingContext(context.Background(), metadata.MD{
		"token": []string{"fuck"},
	})

	newRandomObj := func() string {
		msg, e := c.New(context.Background(), &sp3.Void{})
		if e != nil {
			panic(e)
		}
		newUID := msg.Str
		log.Printf("new random obj: %s", newUID)
		c.Update(ctx, &sp3.UpdateMsg{
			Uid:  newUID,
			From: &sp3.Gobj{},
			To: &sp3.Gobj{
				Name: fmt.Sprintf("name of %s", newUID),
			},
		})
		return msg.Str
	}

	updateAndGet := func(uid string, from, to *sp3.Gobj) *sp3.Gobj {
		c.Update(ctx, &sp3.UpdateMsg{
			Uid:  uid,
			From: from,
			To:   to,
		})

		obj, e := c.GetWithDUID(ctx, &sp3.StringMsg{
			Str: uid,
		})
		if e != nil {
			panic(e)
		}
		return obj
	}

	holidaysMsg, _ := c.Holidays(ctx, &sp3.Void{})
	log.Printf("Holidays: %s", holidaysMsg.Holidays)

	uid := newRandomObj()
	log.Printf("new uid: %s", uid)

	obj := updateAndGet(uid, &sp3.Gobj{}, &sp3.Gobj{
		Name: "a",
		Days: []string{"mon", "tue"},
		Loc: &sp3.Geo{
			Type:        "Point",
			Coordinates: []float64{-122.4220186, 37.772318},
		},
	})

	log.Printf("obj after updating name, days, loc:\n%v", obj)

	obj = updateAndGet(uid, &sp3.Gobj{}, &sp3.Gobj{
		Loc: &sp3.Geo{
			Type:        "Point",
			Coordinates: []float64{1, 2},
		},
	})

	log.Printf("obj after updating loc coordinates :\n%v", obj)

	obj = updateAndGet(uid, &sp3.Gobj{
		Loc: &sp3.Geo{
			Type:        "Point",
			Coordinates: []float64{1, 2},
		},
	}, &sp3.Gobj{})

	log.Printf("obj after removing loc :\n%v", obj)

	obj = updateAndGet(uid, &sp3.Gobj{}, &sp3.Gobj{
		Name: "Fuck",
	})

	log.Printf("obj after blind update name :\n%v", obj)

	relates := []string{newRandomObj(), newRandomObj()}
	log.Printf("relates: %v", relates)
	c.LinkClubs(ctx, &sp3.Linkage{
		Uid: uid,
		To:  relates,
	})
	clubs, _ := c.ClubsOf(ctx, &sp3.StringMsg{
		Str: uid,
	})
	log.Printf("clubs: %v", clubs.Objs)

	searchResult, _ := c.Search(ctx, &sp3.StringMsg{
		Str: "name",
	})
	log.Printf("search result: %v", searchResult.Objs)
}
