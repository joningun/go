FROM golang

RUN go get github.com/parnurzeal/gorequest
RUN go get -u github.com/go-redis/redis
RUN go get -u -v github.com/dgraph-io/dgo
RUN go get firebase.google.com/go
RUN go get -u github.com/gorilla/mux
RUN go get -u github.com/james-bowman/sparse/...
RUN go get -u -t gonum.org/v1/gonum/...
run go get -u google.golang.org/grpc
run go get github.com/wangjia184/sortedset

# Copy the local package files to the container's workspace.
ADD ./server  /go/src/mdfinder.com/server
ADD ./matcher  /go/src/mdfinder.com/matcher

WORKDIR /go/src/mdfinder.com/server
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o out .
WORKDIR /go/src/mdfinder.com/matcher
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o out .
# Run the outyet command by default when the container starts.
#ENTRYPOINT /go/bin/server

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/mdfinder.com/server/out ./server
COPY --from=0 /go/src/mdfinder.com/matcher/out ./matcher

ADD mdfinder-1424c-firebase-adminsdk-tpdh7-f097e55700.json .
#CMD ["./app"]  
# ENV GRPC_GO_LOG_VERBOSITY_LEVEL=99
# ENV GRPC_GO_LOG_SEVERITY_LEVEL=info
# Document that the service listens on port 8080.

EXPOSE 7102 50030
#GRPC_GO_LOG_VERBOSITY_LEVEL=99 GRPC_GO_LOG_SEVERITY_LEVEL=info