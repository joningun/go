package kakao

import (
	"encoding/json"
	"log"

	"github.com/parnurzeal/gorequest"
)

type KakaoMe struct {
	Id         int
	Properties struct {
		Nickname     string
		ProfileImage string `json:"profile_image"`
	}
}

func MeOf(token string) KakaoMe {
	infoKeys := `["properties.nickname", "properties.profile_image"]`
	request := gorequest.New()
	_, body, errs := request.Get("https://kapi.kakao.com/v2/user/me").Set("Authorization", "Bearer "+token).Param("property_keys", infoKeys).End()
	for _, err := range errs {
		panic(err)
	}

	log.Printf("kakao response: %s", body)
	var kme KakaoMe
	e := json.Unmarshal([]byte(body), &kme)
	if e != nil {
		panic(e)
	}
	return kme
}
