package main

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"time"

	"mdfinder.com/server/dimp"
	"mdfinder.com/server/sp3"

	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
)

func toMap(o interface{}) map[string]interface{} {
	bytes, e := json.Marshal(o)
	if e != nil {
		panic(e)
	}
	m := make(map[string]interface{})
	e = json.Unmarshal(bytes, &m)
	if e != nil {
		panic(e)
	}
	return m
}

//Update setJson differences. deepEqual value.
func updateObj(db *dimp.Graph, uid string, from, to interface{}) {
	log.Printf("updateObj called on uid:%s \nfrom: %v\nto: %v", uid, from, to)
	if uid == "" {
		panic("uid is required.")
	}
	fromM := toMap(from)
	toM := toMap(to)

	setm := make(map[string]interface{})
	var delEdges []string

	for k, fromV := range fromM {
		toV, ex := toM[k]
		if ex {
			log.Printf("comparing \n%v\n%v", fromV, toV)
			if !reflect.DeepEqual(fromV, toV) {
				if toV != nil {
					setm[k] = toV
				}
				delEdges = append(delEdges, k)
			}
		} else {
			delEdges = append(delEdges, k)
		}
	}

	for k, toV := range toM {
		_, ex := fromM[k]
		if !ex {
			if toV != nil {
				delEdges = append(delEdges, k)
				setm[k] = toV
			}
		}
	}

	var mutations []*api.Mutation
	setm["uid"] = uid
	setbytes, err := json.Marshal(setm)
	if err != nil {
		panic(err)
	}

	if len(delEdges) > 0 {
		log.Println("deleting edges: ", delEdges)
		delmu := api.Mutation{}
		dgo.DeleteEdges(&delmu, uid, delEdges...)
		mutations = append(mutations, &delmu)
	} else {
		log.Println("No edges to delete")
	}
	log.Printf("set bytes: %s", setbytes)
	mutations = append(mutations, &api.Mutation{
		SetJson: setbytes,
	})

	_ = db.M(mutations)
}

//New create a new object with created time.
func newObj(db *dimp.Graph) string {
	newObj := map[string]time.Time{
		"created": time.Now(),
	}
	bytes, err := json.Marshal(newObj)
	if err != nil {
		panic(err)
	}
	mu := api.Mutation{
		SetJson: bytes,
	}

	assigns := db.M([]*api.Mutation{&mu})

	uid, ex := assigns[0].Uids["blank-0"]
	if !ex {
		panic(fmt.Sprintf("blank-0 not exists. %v", assigns))
	}
	return uid
}

func getByDUID(g *dimp.Graph, uid string) *sp3.Gobj {
	query := fmt.Sprintf(`
	{
		objs(func: uid(%s)) {
			expand(_all_)
		}
	}`, uid)
	raw := g.Q(query)
	log.Printf("GetByDUID %s: \n%s", uid, raw)
	var results map[string][]sp3.Gobj
	umsh(raw, &results)
	objs, ex := results["objs"]
	if !ex {
		log.Printf("objs not exists in query result.")
		return nil
	}
	if len(objs) != 1 {
		log.Printf("object not exists for uid: %s", uid)
		return nil
	}
	return &objs[0]
}

func relatesOf(g *dimp.Graph, uid string, pred string) []sp3.Gobj {
	query := fmt.Sprintf(`
	{
		objs(func: uid(%s)) {
			%s {
				uid
				expand(_all_)
			}
		}
	}`, uid, pred)
	raw := g.Q(query)
	log.Printf("relates %s Of %s: \n%s", pred, uid, raw)
	var results map[string][]map[string][]sp3.Gobj
	umsh(raw, &results)
	objs, ex := results["objs"]
	if !ex {
		panic(fmt.Sprintf("objs not exists in query result."))
	}
	if len(objs) != 1 {
		panic(fmt.Sprintf("object not exists for uid: %s", uid))
	}

	obj := objs[0]
	relates, ex := obj[pred]
	if !ex {
		return []sp3.Gobj{}
	}
	return relates
}

func link(g *dimp.Graph, uid string, pred string, to []string) {
	match := make(map[string]interface{})
	match["uid"] = uid

	uidObjs := deriveFmapUid2UidObj(func(uid string) map[string]string {
		m := make(map[string]string)
		m["uid"] = uid
		return m
	}, to)
	match[pred] = uidObjs
	bytes, e := json.Marshal(match)
	if e != nil {
		panic(e)
	}
	log.Printf("relate setjson: %s", bytes)
	mu := api.Mutation{
		SetJson: bytes,
	}

	_ = g.M([]*api.Mutation{&mu})
}
