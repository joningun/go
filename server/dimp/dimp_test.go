package dimp

import (
	"fmt"
	"log"
	"testing"
)

func Test(t *testing.T) {
	schema := `
		str: string .
		arr: [string] .
		`
	g, _ := NewGraph("localhost:9080", schema)
	uid := g.New()
	from := map[string]interface{}{
		"uid": uid,
		"str": "",
		"arr": []string{},
	}
	to := map[string]interface{}{
		"uid": uid,
		"str": "a",
		"arr": []string{"a", "b"},
	}
	g.Update(from, to)
	qbytes := g.Q(fmt.Sprintf(`
	{
		objs(func: uid(%s)) {
			uid
			expand(_all_)
		}
	}
	`, uid))
	// qmap := make(map[string]interface{})
	// _ = json.Unmarshal(qbytes, &qmap)
	log.Printf("1: %s", qbytes)

	toto := map[string]interface{}{
		"uid": uid,
		"str": "b",
		"arr": []string{"a", "b"},
	}
	g.Update(to, toto)
	qbytes = g.Q(fmt.Sprintf(`
	{
		objs(func: uid(%s)) {
			uid
			expand(_all_)
		}
	}
	`, uid))
	// qmap := make(map[string]interface{})
	// _ = json.Unmarshal(qbytes, &qmap)
	log.Printf("2: %s", qbytes)

	tototo := map[string]interface{}{
		"uid": uid,
		"str": "b",
		"arr": []string{"f", "u", "c", "k"},
	}
	g.Update(toto, tototo)
	qbytes = g.Q(fmt.Sprintf(`
	{
		objs(func: uid(%s)) {
			uid
			expand(_all_)
		}
	}
	`, uid))
	// qmap := make(map[string]interface{})
	// _ = json.Unmarshal(qbytes, &qmap)
	log.Printf("3: %s", qbytes)
}
