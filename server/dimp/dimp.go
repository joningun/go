package dimp

import (
	"context"
	"encoding/json"
	"log"
	"sync"

	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
	"google.golang.org/grpc"
)

type Graph struct {
	dg        *dgo.Dgraph
	upsertMux *sync.Mutex
}

func NewGraph(addr, schema string) (*Graph, error) {
	d, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	dg := dgo.NewDgraphClient(
		api.NewDgraphClient(d),
	)

	err = dg.Alter(context.Background(), &api.Operation{
		Schema: schema,
	})

	if err != nil {
		return nil, err
	}
	txn := dg.NewTxn()
	defer txn.Discard(context.Background())

	rsp, err := txn.Query(context.Background(), `schema { type }`)
	if err != nil {
		panic(err)
	}
	schemaNodes := rsp.GetSchema()
	predicateMap := make(map[string]string)
	for idx, node := range schemaNodes {
		pred, tp, tkns := node.GetPredicate(), node.GetType(), node.GetTokenizer()
		log.Printf("Schema %d: %s, %s, %v", idx, pred, tp, len(tkns))

		predicateMap[pred] = tp
	}
	txn.Commit(context.Background())

	return &Graph{dg: dg, upsertMux: &sync.Mutex{}}, nil
}

//Q perform query and return raw json bytes.
func (graph *Graph) Q(query string) json.RawMessage {
	txn := graph.dg.NewTxn()
	defer txn.Discard(context.Background())
	rsp, err := txn.Query(context.Background(), query)
	if err != nil {
		panic(err)
	}
	txn.Commit(context.Background())
	return rsp.GetJson()
}

//M perform mutations.
func (graph *Graph) M(mutations []*api.Mutation) []*api.Assigned {
	assigns := make([]*api.Assigned, 0, len(mutations))
	txn := graph.dg.NewTxn()
	for _, m := range mutations {
		assigned, e := txn.Mutate(context.Background(), m)
		if e != nil {
			panic(e)
		}
		assigns = append(assigns, assigned)
	}
	e := txn.Commit(context.Background())
	if e != nil {
		panic(e)
	}
	return assigns
}

// func (graph *Graph) M(mu *api.Mutation) *api.Assigned {
// 	txn := graph.dg.NewTxn()
// 	mu.CommitNow = true
// 	assign, err := txn.Mutate(context.Background(), mu)
// 	if err != nil {
// 		panic(err)
// 	}
// 	log.Println("mutation!")
// 	return assign
// }

// func (graph *Graph) Get(uid string) []byte {
// 	query := fmt.Sprintf(`{
// 		objs(func: uid(%s)) {
// 			uid
// 			expand(_all_) {uid expand(_all_)}
// 		}
// 	}
// 	`, uid)
// 	txn := graph.dg.NewTxn()
// 	defer txn.Discard(context.Background())
// 	var v struct {
// 		Objs []json.RawMessage
// 	}
// 	rsp, err := txn.Query(context.Background(), query)
// 	if err != nil {
// 		panic(err)
// 	}
// 	if err = json.Unmarshal(rsp.GetJson(), &v); err != nil {
// 		panic(err)
// 	}
// 	//log.Printf("query result %s", v.Objs)
// 	txn.Commit(context.Background())
// 	return v.Objs[0]
// }
// func (graph *Graph) Update(setbytes []byte, delbytes []byte) {
// 	if len(delbytes) > 0 {
// 		var dmap map[string]json.RawMessage
// 		e := json.Unmarshal(delbytes, &dmap)
// 		if e != nil {
// 			panic(e)
// 		}
// 		if len(dmap) == 0 || len(dmap) == 1 {
// 			delbytes = []byte("")
// 		}
// 	}
// 	log.Printf("final delbytes: %s", delbytes)
// 	graph.mutate(&api.Mutation{SetJson: setbytes, DeleteJson: delbytes})
// }

// func (graph *Graph) Delete(uid string) {
// 	graph.mutate(&api.Mutation{DelNquads: []byte(fmt.Sprintf("<%s> * * .", uid))})
// }
