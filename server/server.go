package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"runtime"
	"strconv"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"mdfinder.com/server/dimp"
	"mdfinder.com/server/fireImp"
	"mdfinder.com/server/holiday"
	"mdfinder.com/server/kakao"
	"mdfinder.com/server/redi"
	"mdfinder.com/server/sp3"
)

type rpc struct {
	Fire  *fireImp.Fire
	hf    *holiday.HolidayFetcher
	redis *redi.Redi
	graph *dimp.Graph
}

func (r *rpc) Hit(ctx context.Context, hitmsg *sp3.HitMsg) (*sp3.Void, error) {
	log.Printf("hit: %s, %s", hitmsg.Searched, hitmsg.Hit)
	r.redis.Hit(hitmsg.Searched, hitmsg.Hit)
	return &sp3.Void{}, nil
}

func (r *rpc) Autocomplete(ctx context.Context, strmsg *sp3.StringMsg) (*sp3.StringsMsg, error) {
	token := strmsg.Str
	log.Printf("autocomplete: %s", token)
	return &sp3.StringsMsg{
		Strs: r.redis.Autocomplete(token),
	}, nil
}

func (r *rpc) KakaoLogin(ctx context.Context, token *sp3.StringMsg) (*sp3.StringMsg, error) {
	kakaoToken := token.Str
	kakaoMe := kakao.MeOf(kakaoToken)
	log.Printf("unmarshaled kakaoMe : %s", kakaoMe)
	uid := strconv.Itoa(kakaoMe.Id)

	r.Fire.UpdateUser(uid, kakaoMe.Properties.Nickname, kakaoMe.Properties.ProfileImage)

	customToken, e := r.Fire.Auth.CustomToken(context.Background(), uid)
	if e != nil {
		panic(e)
	}
	return &sp3.StringMsg{Str: customToken}, nil
}

func (r *rpc) newUser(fireUid string) string {
	user := r.Fire.GetUser(fireUid)
	duid := newObj(r.graph)
	from := sp3.Gobj{
		Uid: duid,
	}
	to := sp3.Gobj{
		Uid:   duid,
		Fid:   user.UID,
		Name:  user.DisplayName,
		Photo: user.PhotoURL,
	}
	updateObj(r.graph, duid, from, to)
	log.Printf("New Assign: %s - %s", user.UID, duid)
	r.Fire.SetClaim(user.UID, "duid", duid)
	return duid
}

func (r *rpc) Login(ctx context.Context, _ *sp3.Void) (*sp3.StringMsg, error) {
	log.Println("login called!")
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		log.Println("no metadata exists.")
		return nil, errors.New("fuck")
	}
	log.Printf("md: %v", md)
	token := md["token"][0]
	log.Println("login attempt: ", token)

	tk := r.Fire.VerifyToken(token)

	duid := r.Fire.GetUserClaim(tk.UID, "duid")
	if duid == "" {
		log.Printf("Assigning new user.")
		duid = r.newUser(tk.UID)
	} else {
		if o := getByDUID(r.graph, duid); o != nil {
			if o.Fid != tk.UID {
				log.Printf("Fid-UID Mismatch. Assigning new user.")
				duid = r.newUser(tk.UID)
			} else {

			}
		} else {
			log.Printf("UID not exists. Assigning new user.")
			duid = r.newUser(tk.UID)
		}

	}

	return &sp3.StringMsg{Str: duid}, nil
}

//go:generate goderive .
func mapHolidayOut(h holiday.Holiday) *sp3.Holiday {
	return &sp3.Holiday{
		Name: h.Name,
		Date: h.Date.Format(time.RFC3339),
	}
}
func (r *rpc) Holidays(context.Context, *sp3.Void) (*sp3.HolidaysMsg, error) {
	holidays := r.hf.Holidays()
	outHolidays := deriveFmapHolidayOut(mapHolidayOut, holidays)
	return &sp3.HolidaysMsg{
		Holidays: outHolidays,
	}, nil
}

func (r *rpc) GetWithDUID(_ context.Context, msg *sp3.StringMsg) (*sp3.Gobj, error) {
	uid := msg.Str
	return getByDUID(r.graph, uid), nil
}

func (r *rpc) Update(_ context.Context, msg *sp3.UpdateMsg) (*sp3.Void, error) {
	updateObj(r.graph, msg.Uid, msg.From, msg.To)
	return &sp3.Void{}, nil
}

func (r *rpc) New(context.Context, *sp3.Void) (*sp3.StringMsg, error) {
	uid := newObj(r.graph)
	return &sp3.StringMsg{
		Str: uid,
	}, nil
}

func o2p(o sp3.Gobj) *sp3.Gobj {
	return &o
}
func (r *rpc) ClubsOf(_ context.Context, msg *sp3.StringMsg) (*sp3.Gobjs, error) {
	clubs := relatesOf(r.graph, msg.Str, "clubs")
	return &sp3.Gobjs{
		Objs: deriveFmapPointer(o2p, clubs),
	}, nil
}

func (r *rpc) LinkClubs(_ context.Context, msg *sp3.Linkage) (*sp3.Void, error) {
	link(r.graph, msg.Uid, "clubs", msg.To)
	return &sp3.Void{}, nil
}

func (r *rpc) FlyersOf(_ context.Context, msg *sp3.StringMsg) (*sp3.Gobjs, error) {
	clubs := relatesOf(r.graph, msg.Str, "flyers")
	return &sp3.Gobjs{
		Objs: deriveFmapPointer(o2p, clubs),
	}, nil
}

func (r *rpc) LinkFlyers(_ context.Context, msg *sp3.Linkage) (*sp3.Void, error) {
	link(r.graph, msg.Uid, "flyers", msg.To)
	return &sp3.Void{}, nil
}

func (r *rpc) Search(_ context.Context, msg *sp3.StringMsg) (*sp3.Gobjs, error) {
	term := msg.Str
	log.Printf("Search called: %s", term)
	query := fmt.Sprintf(`
	{
		objs(func: regexp(name, /%s/i)) {
			uid
			expand(_all_)
		}
	}`, term)
	raw := r.graph.Q(query)
	log.Printf("Search result: %s", raw)
	match := make(map[string][]sp3.Gobj)
	e := json.Unmarshal(raw, &match)
	if e != nil {
		panic(e)
	}
	return &sp3.Gobjs{
		Objs: deriveFmapPointer(o2p, match["objs"]),
	}, nil
}

const cDgraphAddr string = "dgraph:9080"
const cRediAddr string = "redis:6379"

func rpcthing(hf *holiday.HolidayFetcher) {
	dgraphAddr := flag.String("dgraph", cDgraphAddr, "dgraph <host>:<port>")
	rediAddr := flag.String("redis", cRediAddr, "redis <host>:<port>")
	flag.Parse()
	log.Println("Dgraph address: ", *dgraphAddr)
	log.Println("Redis address: ", *rediAddr)
	fire, err := fireImp.FirebaseApp()
	if err != nil {
		panic(err)
	}
	g, err := dimp.NewGraph(*dgraphAddr, `
		fid: string .
		name: string @index(trigram) .
		desc: string .
		photo: string .
		loc: geo @index(geo) .
		days: [string] @index(exact) .
		opens: [dateTime] @index(day) .
		closings: [dateTime] @index(day) .
		clubs: uid @reverse .
		flyers: uid @reverse .
	`)

	if err != nil {
		panic(err)
	}
	lis, err := net.Listen("tcp", ":50030")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	//creds, _ := credentials.NewServerTLSFromFile("/fullchain1.pem", "/privkey1.pem")
	//s := grpc.NewServer(grpc.Creds(creds))
	s := grpc.NewServer()
	sp3.RegisterServerServer(s, &rpc{
		hf:    hf,
		Fire:  fire,
		redis: redi.New(*rediAddr),
		graph: g,
	})
	// Register reflection service on gRPC server.
	reflection.Register(s)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
func main() {

	log.Println("go version: ", runtime.Version())

	holidayFetcher := holiday.NewHolidayFetcher(24 * time.Hour)
	rpcthing(holidayFetcher)

	// router.HandleFunc("/query/{encoded}", func(w http.ResponseWriter, r *http.Request) {
	// 	vars := mux.Vars(r)
	// 	encodedJson := vars["encoded"]
	// 	base64Json, e := url.PathUnescape(encodedJson)
	// 	if e != nil {
	// 		panic(e)
	// 	}
	// 	decodedJson, e := b64.StdEncoding.DecodeString(base64Json)
	// 	if e != nil {
	// 		panic(e)
	// 	}
	// 	log.Printf("query json: %s", decodedJson)
	// 	writeJson(w, dm.Query(decodedJson))
	// }).Methods("GET")

	// router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	dm.Update(bodyBytes(r))
	// 	w.WriteHeader(http.StatusOK)
	// }).Methods("PUT")

	// router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	obj := dm.New(bodyBytes(r))
	// 	writeJson(w, marshal(obj))
	// }).Methods("POST")

	// router.HandleFunc("/{uid}", func(w http.ResponseWriter, r *http.Request) {
	// 	vars := mux.Vars(r)
	// 	uid := vars["uid"]
	// 	log.Println("get uid: ", uid)
	// 	writeJson(w, dm.Get(uid))
	// }).Methods("GET")

	// log.Fatal(http.ListenAndServe(":7102", router))
}

type UidObj struct {
	Uid string `json:"uid"`
}

func writeJson(w http.ResponseWriter, bytes []byte) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
}

func writeString(w http.ResponseWriter, content string) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(content))
}
func umsh(bytes []byte, p interface{}) {
	e := json.Unmarshal(bytes, p)
	if e != nil {
		panic(e)
	}
}
