package holiday

import (
	"log"
	"testing"
	"time"
)

func TestHoli(t *testing.T) {
	aa := NewHolidayFetcher(20 * time.Second)
	holidays := aa.Holidays()
	log.Println("0:  called holiday. result: ", len(holidays), " ...")
	_ = <-time.After(5 * time.Second)
	holidays = aa.Holidays()
	log.Println("5:  called holiday. result: ", len(holidays), " ...")
	_ = <-time.After(10 * time.Second)
	holidays = aa.Holidays()
	log.Println("15: called holiday. result: ", len(holidays), " expecting a refresh next...")
	_ = <-time.After(10 * time.Second)
	holidays = aa.Holidays()
	log.Println("25: called holiday. result: ", len(holidays), " was there a refresh?")
	log.Println(holidays)
}
