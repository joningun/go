package holiday

import (
	"encoding/json"
	"log"
	"strconv"
	"sync"
	"time"

	"github.com/parnurzeal/gorequest"
)

type Holiday struct {
	Name string    `json:"name"`
	Date time.Time `json:"date"`
}

type HolidayFetcher struct {
	cache []Holiday
	mux   sync.Mutex
}

func worker(fetcher *HolidayFetcher, term time.Duration) {
	for {
		select {
		case <-time.After(term):
			log.Println("1 day passed. refreshing holiday list")
			newHolidays := upcomingKrHolidays()
			fetcher.mux.Lock()
			fetcher.cache = newHolidays
			fetcher.mux.Unlock()
		}
	}
}

func NewHolidayFetcher(reloadTerm time.Duration) *HolidayFetcher {
	fetcher := HolidayFetcher{cache: upcomingKrHolidays()}
	go worker(&fetcher, reloadTerm)
	return &fetcher
}

func (fetcher *HolidayFetcher) Holidays() []Holiday {
	fetcher.mux.Lock()
	defer fetcher.mux.Unlock()
	return fetcher.cache
}

func upcomingKrHolidays() []Holiday {
	years := []int{time.Now().Year(), time.Now().Year() + 1}
	daytypes := []string{"h", "i"}

	var holidayResponse struct {
		TotalResult int
		Results     []map[string]string
	}
	holidays := make([]Holiday, 0)
	for _, year := range years {
		for _, daytype := range daytypes {
			request := gorequest.New()
			_, body, errs := request.Get("https://apis.sktelecom.com/v1/eventday/days").
				Set("TDCProjectkey", "3b88019a-7c2b-4383-96c2-3552cd8d0c2f").
				Param("type", daytype).Param("year", strconv.Itoa(year)).End()
			for _, err := range errs {
				panic(err)
			}
			if err := json.Unmarshal([]byte(body), &holidayResponse); err != nil {
				panic(err)
			}
			for _, result := range holidayResponse.Results {
				date, err := time.Parse("20060102", result["year"]+result["month"]+result["day"])
				if err != nil {
					log.Println("failed parsing holiday: ", result)
					continue
				}
				holi := Holiday{Name: result["name"], Date: date}
				holidays = append(holidays, holi)
			}
		}
	}
	return holidays
}
