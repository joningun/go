package redi

import (
	"fmt"
	"log"
	"strings"
	"unicode/utf8"

	"github.com/go-redis/redis"
)

type Redi struct {
	client *redis.Client
}

func New(addr string) *Redi {
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	pong, err := client.Ping().Result()
	fmt.Println("redis ping: ", pong, err)
	if err != nil {
		panic(err)
	}
	return &Redi{client: client}
}
func tokensOf(str string) []string {
	fields := strings.Fields(str)
	var tokens []string
	for _, field := range fields {
		l := utf8.RuneCountInString(field)
		log.Printf("field: (%d)%s", l, field)
		runes := []rune(field)
		for i := 0; i < l; i++ {
			sub := string(runes[:l-i])
			log.Printf(sub)
			tokens = append(tokens, sub)
		}
	}
	log.Printf("tokens of %s: %s", str, tokens)
	return tokens
}
func (redi *Redi) Coin(coin string) {
	if coin == "" {
		return
	}
	exists, e := redi.client.SIsMember("coins", coin).Result()
	if e != nil {
		return
	}
	if exists {
		return
	}
	tkns := tokensOf(coin)
	pipeliner := redi.client.Pipeline()
	for _, tkn := range tkns {
		z := redis.Z{0, coin}
		pipeliner.ZAdd("token:"+strings.ToLower(tkn), z)
	}
	pipeliner.SAdd("coins", coin)
	_, e = pipeliner.Exec()
	if e != nil {
		panic(e)
	}
}
func (redi *Redi) Autocomplete(token string) []string {
	results, e := redi.client.ZRevRange("token:"+strings.ToLower(token), 0, 10).Result()
	log.Printf("autocomplete result: %s", results)
	if e != nil {
		panic(e)
	}
	return results
}
func (redi *Redi) Hit(searched, hit string) {
	score, e := redi.client.ZIncrXX("token:"+searched, redis.Z{Score: 1, Member: hit}).Result()
	log.Printf("score for %s-%s: %f", searched, hit, score)
	if e != nil {
		log.Fatal("hit fail: ", e)
	}
}
