package main

import (
	"fmt"
	"log"
	"testing"

	"mdfinder.com/server/dimp"
)

type A struct {
	Uid string   `json:"uid"`
	Str string   `json:"str"`
	Arr []string `json:"arr"`
}

func Test(t *testing.T) {
	schema := `
		str: string .
		arr: [string] .
		`
	g, _ := dimp.NewGraph("localhost:9080", schema)
	uid := newObj(g)
	printInfo(g, uid)
	from := A{
		Uid: uid,
		Str: "",
		Arr: []string{},
	}
	to := A{
		Uid: uid,
		Str: "a",
		Arr: []string{"a"},
	}
	updateObj(g, &from, &to)
	printInfo(g, uid)
	from = to
	to.Str = "b"
	to.Arr = []string{"b"}
	updateObj(g, from, to)
	printInfo(g, uid)
}

func printInfo(g *dimp.Graph, uid string) {
	qbytes := g.Q(fmt.Sprintf(`
	{
		objs(func: uid(%s)) {
			uid
			expand(_all_)
		}
	}
	`, uid))
	// qmap := make(map[string]interface{})
	// _ = json.Unmarshal(qbytes, &qmap)
	log.Printf("1: %s", qbytes)
}
