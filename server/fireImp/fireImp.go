package fireImp

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	fireauth "firebase.google.com/go/auth"

	"google.golang.org/api/option"
)

type Fire struct {
	App   *firebase.App
	Auth  *fireauth.Client
	Store *firestore.Client
}

func FirebaseApp() (*Fire, error) {
	opt := option.WithCredentialsFile("./mdfinder-1424c-firebase-adminsdk-tpdh7-f097e55700.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return nil, err
	}
	auth, err := app.Auth(context.Background())
	if err != nil {
		return nil, err
	}
	store, err := app.Firestore(context.Background())
	if err != nil {
		return nil, err
	}
	return &Fire{app, auth, store}, nil
}

func (fire *Fire) UpdateUser(uid string, displayName string, photo string) {
	_, e := fire.Auth.GetUser(context.Background(), uid)
	if e != nil {
		if fireauth.IsUserNotFound(e) {
			log.Printf("User To Update Not Found. creating new one.")
			u := (&fireauth.UserToCreate{}).PhotoURL(photo).DisplayName(displayName).UID(uid)
			_, e = fire.Auth.CreateUser(context.Background(), u)
			if e != nil {
				panic(e)
			}
		} else {
			panic(e)
		}
	} else {
		params := (&fireauth.UserToUpdate{}).
			DisplayName(displayName).
			PhotoURL(photo)
		_, e = fire.Auth.UpdateUser(context.Background(), uid, params)
		if e != nil {
			panic(e)
		}
	}
}

type AuthFail struct {
	fireErr error
}

func (a AuthFail) Error() string {
	return "auth fail u need to re-login again mmmhm. fire err msg: " + a.fireErr.Error()
}

func (fire *Fire) VerifyToken(token string) *fireauth.Token {
	decoded, e := fire.Auth.VerifyIDToken(context.Background(), token)
	if e != nil {
		log.Println("Firebase token verification fail.")
		panic(AuthFail{fireErr: e})
	}
	return decoded
}
func (fire *Fire) GetUser(uid string) *fireauth.UserRecord {
	rec, e := fire.Auth.GetUser(context.Background(), uid)
	if e != nil {
		panic(e)
	}
	return rec
}
func (fire *Fire) GetTokenClaim(token *fireauth.Token, key string) string {
	v, exist := token.Claims[key]
	if exist {
		return v.(string)
	} else {
		return ""
	}
}

func (fire *Fire) GetUserClaim(uid string, key string) string {
	record, e := fire.Auth.GetUser(context.Background(), uid)
	if e != nil {
		panic(e)
	}
	v, exist := record.CustomClaims[key]
	if exist {
		return v.(string)
	} else {
		return ""
	}
}

func (fire *Fire) SetClaim(uid string, key string, v string) {
	claim := map[string]interface{}{key: v}
	e := fire.Auth.SetCustomUserClaims(context.Background(), uid, claim)
	if e != nil {
		panic(e)
	}
}

type Feed struct {
	From string    `firestore:"from" json:"from"`
	Text string    `firestore:"text" json:"text"`
	Date time.Time `firestore:"date" json:"date"`
}

func (fire *Fire) SendChat(roomname string, from string, text string) {
	feed := Feed{From: from, Text: text, Date: time.Now()}
	fire.Store.Collection("chatrooms").Doc(roomname).Collection("chats").Add(context.Background(), feed)

}

func (fire *Fire) BiChatSent(data []byte, to string) {
	var feed Feed
	e := json.Unmarshal(data, &feed)
	if e != nil {
		panic(e)
	}
	fire.Store.Collection("boards").Doc(to).Collection("board").Doc(feed.From).Set(context.Background(), feed)
	fire.Store.Collection("boards").Doc(feed.From).Collection("board").Doc(to).Set(context.Background(), feed)
}
