package matcher

import (
	"github.com/james-bowman/sparse"
)

type wedge struct {
	A, B int
	W    int64
}

type calable interface {
	calFeatures() []Feature
}

func cal(left []calable, right []calable) []wedge { //left, right - samples
	var allFeatures []string
	tbl := make(map[string]int) //indices of feature in row

	for _, samples := range [][]calable{left, right} {
		for _, sample := range samples {
			for _, feature := range sample.calFeatures() {
				_, exist := tbl[feature.Key]
				if !exist {
					tbl[feature.Key] = len(allFeatures)
					allFeatures = append(allFeatures, feature.Key)
				}
			}

		}
	}

	leftDok := sparse.NewDOK(len(left), len(allFeatures))
	rightDok := sparse.NewDOK(len(allFeatures), len(right))

	for idx, isample := range left {
		for _, feature := range isample.calFeatures() {
			leftDok.Set(idx, tbl[feature.Key], float64(feature.Plus))
		}
	}

	for idx, isample := range right {
		for _, feature := range isample.calFeatures() {
			rightDok.Set(tbl[feature.Key], idx, float64(feature.Plus))
		}
	}

	var csrProduct sparse.CSR
	csrProduct.Mul(leftDok.ToCSR(), rightDok.ToCSR())

	var edges []wedge
	csrProduct.DoNonZero(func(i, j int, w float64) {
		edges = append(edges, wedge{i, j, int64(w)})
	})
	return edges
}
