package simg

import (
	"sync"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/simple"
)

type TSGraph struct {
	m  sync.RWMutex
	db *simple.DirectedGraph
	W  *tsGraphW
	R  *tsGraphR
}

type graphReader interface {
	graph.Directed
}
type tsGraphR struct {
	m sync.Locker
	g graphReader
}

type graphWriter interface {
	graph.NodeAdder
	graph.NodeRemover
	graph.EdgeAdder
}
type tsGraphW struct {
	m *sync.RWMutex
	g graphWriter
}

func New() *TSGraph {
	g := &TSGraph{
		db: simple.NewDirectedGraph(),
	}
	g.W = &tsGraphW{
		m: &g.m,
		g: g.db,
	}
	g.R = &tsGraphR{
		m: g.m.RLocker(),
		g: g.db,
	}
	return g
}

func (g *tsGraphW) NewNode() graph.Node {
	g.m.Lock()
	defer g.m.Unlock()
	return g.g.NewNode()
}

func (g *tsGraphW) AddNode(n graph.Node) {
	g.m.Lock()
	defer g.m.Unlock()
	g.g.AddNode(n)
}

func (g *tsGraphW) RemoveNode(id int64) {
	g.m.Lock()
	defer g.m.Unlock()
	g.g.RemoveNode(id)
}

func (g *tsGraphW) NewEdge(from graph.Node, to graph.Node) graph.Edge {
	g.m.Lock()
	defer g.m.Unlock()
	return g.g.NewEdge(from, to)
}

func (g *tsGraphW) SetEdge(e graph.Edge) {
	g.m.Lock()
	defer g.m.Unlock()
	g.g.SetEdge(e)
}

func (g *tsGraphR) Node(id int64) graph.Node {
	g.m.Lock()
	defer g.m.Unlock()
	return g.g.Node(id)
}

func (g *tsGraphR) Nodes() graph.Nodes {
	g.m.Lock()
	defer g.m.Unlock()
	return g.g.Nodes()

}

func (g *tsGraphR) From(id int64) graph.Nodes {
	g.m.Lock()
	defer g.m.Unlock()
	return g.g.From(id)
}

func (g *tsGraphR) Edge(uid int64, vid int64) graph.Edge {
	g.m.Lock()
	defer g.m.Unlock()
	return g.g.Edge(uid, vid)
}

func (g *tsGraphR) To(id int64) graph.Nodes {
	g.m.Lock()
	defer g.m.Unlock()
	return g.g.To(id)
}
