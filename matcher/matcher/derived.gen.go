// Code generated by goderive DO NOT EDIT.

package matcher

import (
	sortedset "github.com/wangjia184/sortedset"
	glayer "mdfinder.com/matcher/matcher/glayer"
)

// deriveFmap_IDable_Sample returns a list where each element of the input list has been morphed by the input function.
func deriveFmap_IDable_Sample(f func(glayer.IDable) Sample, list []glayer.IDable) []Sample {
	out := make([]Sample, len(list))
	for i, elem := range list {
		out[i] = f(elem)
	}
	return out
}

// deriveFmap_Node_Group returns a list where each element of the input list has been morphed by the input function.
func deriveFmap_Node_Group(f func(*sortedset.SortedSetNode) Group, list []*sortedset.SortedSetNode) []Group {
	out := make([]Group, len(list))
	for i, elem := range list {
		out[i] = f(elem)
	}
	return out
}

// deriveFmap_Sample_ID returns a list where each element of the input list has been morphed by the input function.
func deriveFmap_Sample_ID(f func(Sample) string, list []Sample) []string {
	out := make([]string, len(list))
	for i, elem := range list {
		out[i] = f(elem)
	}
	return out
}

// deriveFmap_Sample_Calable returns a list where each element of the input list has been morphed by the input function.
func deriveFmap_Sample_Calable(f func(Sample) calable, list []Sample) []calable {
	out := make([]calable, len(list))
	for i, elem := range list {
		out[i] = f(elem)
	}
	return out
}

// deriveFmap_IDable_Calable returns a list where each element of the input list has been morphed by the input function.
func deriveFmap_IDable_Calable(f func(glayer.IDable) calable, list []glayer.IDable) []calable {
	out := make([]calable, len(list))
	for i, elem := range list {
		out[i] = f(elem)
	}
	return out
}
