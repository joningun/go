package glayer

import (
	"mdfinder.com/matcher/matcher/simg"
)

type GraphDataArrow struct {
	db   *simg.TSGraph
	from *GraphDataLayer
	to   *GraphDataLayer
}

func NewArrow(from, to *GraphDataLayer) *GraphDataArrow {
	if from.g != to.g {
		panic("from and to must hv same DB")
	}
	return &GraphDataArrow{
		db:   from.g,
		from: from,
		to:   to,
	}
}
func (arrow *GraphDataArrow) objsFrom(id int64) (objs []IDable) {
	nodes := arrow.db.R.From(id)
	//copy for safety
	for nodes.Next() {
		objs = append(objs, arrow.to.ValueOfNode(nodes.Node().ID()))
	}
	return
}
func (arrow *GraphDataArrow) ObjsFrom(id string) (objs []IDable) {
	return arrow.objsFrom(arrow.from.nodeID(id))
}

func (arrow *GraphDataArrow) objsTo(id int64) (objs []IDable) {
	nodes := arrow.db.R.To(id)
	//copy for safety
	for nodes.Next() {
		objs = append(objs, arrow.from.ValueOfNode(nodes.Node().ID()))
	}
	return
}
func (arrow *GraphDataArrow) ObjsTo(id string) (objs []IDable) {
	return arrow.objsTo(arrow.to.nodeID(id))
}

func (arrow *GraphDataArrow) Link(from, to string) {
	edge := arrow.db.W.NewEdge(
		arrow.db.R.Node(arrow.from.nodeID(from)),
		arrow.db.R.Node(arrow.to.nodeID(to)),
	)
	arrow.db.W.SetEdge(edge)
}

func (arrow *GraphDataArrow) LonelyToObjs() (alones []IDable) {
	for _, nodeID := range arrow.to.nodes() {
		if len(arrow.objsTo(nodeID)) == 0 {
			alones = append(alones, arrow.to.ValueOfNode(nodeID))
		}
	}
	return
}
