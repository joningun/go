package glayer

import (
	"sync"

	"mdfinder.com/matcher/matcher/simg"
)

type GraphDataLayer struct {
	g    *simg.TSGraph
	nTbl map[int64]IDable
	iTbl map[string]int64
	m    sync.RWMutex
}

func New(g *simg.TSGraph) *GraphDataLayer {
	return &GraphDataLayer{
		g:    g,
		nTbl: make(map[int64]IDable),
		iTbl: make(map[string]int64),
	}
}

//Add new or overwrite.
func (ass *GraphDataLayer) Add(v IDable) {
	ass.m.Lock()
	defer ass.m.Unlock()
	nodeid, ex := ass.iTbl[v.ID()]
	if ex {
		ass.nTbl[nodeid] = v
		return
	}
	n := ass.g.W.NewNode()
	ass.g.W.AddNode(n)
	ass.nTbl[n.ID()] = v
	ass.iTbl[v.ID()] = n.ID()
}

//Rm remove or skip if not exists.
func (ass *GraphDataLayer) Rm(id string) {
	ass.m.Lock()
	defer ass.m.Unlock()
	nodeID, ex := ass.iTbl[id]
	if !ex {
		return
	}
	delete(ass.iTbl, id)
	delete(ass.nTbl, nodeID)
	ass.g.W.RemoveNode(nodeID)
}

func (ass *GraphDataLayer) nodeID(id string) int64 {
	ass.m.RLock()
	defer ass.m.RUnlock()
	return ass.iTbl[id]
}

func (ass *GraphDataLayer) ValueOfNode(id int64) IDable {
	ass.m.RLock()
	defer ass.m.RUnlock()
	return ass.nTbl[id]
}
func (ass *GraphDataLayer) ValueOfID(id string) IDable {
	ass.m.RLock()
	defer ass.m.RUnlock()
	if n, b := ass.iTbl[id]; b {
		if s, b := ass.nTbl[n]; b {
			return s
		}
	}
	return nil
}
func (ass *GraphDataLayer) Values() (shags []IDable) {
	ass.m.RLock()
	defer ass.m.RUnlock()
	for _, b := range ass.nTbl {
		shags = append(shags, b)
	}
	return
}
func (ass *GraphDataLayer) nodes() (shags []int64) {
	ass.m.RLock()
	defer ass.m.RUnlock()
	for i := range ass.nTbl {
		shags = append(shags, i)
	}
	return
}

type IDable interface {
	ID() string
}
