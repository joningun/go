package matcher

import (
	"fmt"
	"log"
	"math/rand"
	"sort"
	"sync"
	"time"

	"mdfinder.com/matcher/matcher/glayer"
)

//Matcher asd
type Matcher struct {
	DB      *GG
	started bool
	rate    int64
	mutex   *sync.Mutex
}

//NewGroup asdf

func New(refreshRate int64) *Matcher {
	rand.Seed(time.Now().Unix())
	return &Matcher{
		DB:      NewGG(),
		started: false, rate: refreshRate,
		mutex: &sync.Mutex{}}
}

func (matcher *Matcher) QueueSample(sam Sample) {
	matcher.DB.AddSamples([]Sample{sam})
}

const ( // iota is reset to 0
	StatMatched = iota // c0 == 0
	c1          = iota // c1 == 1
	c2          = iota // c2 == 2
)

func (matcher *Matcher) Start() {
	if matcher.started {
		return
	}
	go func() {
		for {
			start := time.Now()
			matcher.flushSampleQueue()
			tooktime := time.Since(start)
			if tooktime > time.Second {
				fmt.Println("Matching took %f seconds", tooktime.Seconds())
			}

			time.Sleep(time.Duration(matcher.rate) * time.Second)
		}
	}()
	matcher.started = true
}

//Unmatch queue
func (matcher *Matcher) Unmatch(id string) {
	log.Println("unmatching: ", id)
	matcher.DB.RemoveSample(id)
}
func (matcher *Matcher) DirectMatch(sample Sample, groupID string) {
	if existing := matcher.DB.SampleOf(sample.Id); existing != nil {
		panic("rejected: sample id already exists")
	}
	matcher.DB.AddSamples([]Sample{sample})
	matcher.DB.JoinSampleToGroup(sample.Id, groupID)
}

//go:generate goderive .
func (matcher *Matcher) flushSampleQueue() {
	alones := matcher.DB.LonelySamples()

	if len(alones) > 0 {
		log.Printf("alone samples: %v", deriveFmap_Sample_ID(func(s Sample) string { return s.Id }, alones))
	}
	db := matcher.DB

	idSample := func(s Sample) calable {
		return s
	}
	idGroup := func(g glayer.IDable) calable {
		return g.(Group)
	}
	newSampleCals := deriveFmap_Sample_Calable(idSample, alones)
	groupCals := deriveFmap_IDable_Calable(idGroup, matcher.DB.groups.Values())
	sumCals := append(newSampleCals, groupCals...)
	newEdges := cal(newSampleCals, sumCals)
	sort.Slice(newEdges, func(idx1, idx2 int) bool {
		return newEdges[idx1].W > newEdges[idx2].W
	})

	newJoinTbl := make(map[string]string)
	join := func(sample Sample, groupID string) {
		db.JoinSampleToGroup(sample.Id, groupID)
		newJoinTbl[sample.Id] = groupID
	}
	for _, edge := range newEdges {
		a, b := edge.A, edge.B
		newSample, right := newSampleCals[a].(Sample), sumCals[b]
		if _, alreadyJoined := newJoinTbl[newSample.Id]; alreadyJoined {
			continue
		}
		switch v := right.(type) {
		case Group:
			if db.GetNumberOfSamplesInGroup(v.id) < 5 {
				join(newSample, v.id)
				log.Printf("Join (%d): %s --> %s", edge.W, newSample.Id, v.id)
			} else {
				log.Printf("Reject: %s -X-> %s", newSample.Id, v.id)
			}
		case Sample:
			if newSample.Id != v.Id {
				if g, ex := newJoinTbl[v.Id]; ex {
					if db.GetNumberOfSamplesInGroup(g) < 5 {
						join(newSample, g)
						log.Printf("Follow (%d): %s --> %s ", edge.W, newSample.Id, v.Id)
					}
				} else {
					totalFeatures := append(newSample.Features, v.Features...)
					g := NewGroupWithRandomID(totalFeatures)
					db.AddGroup(g)
					join(newSample, g.id)
					join(v, g.id)
					log.Printf("New (%d): %s --> %s <-- %s ", edge.W, newSample.Id, g.id, v.Id)
				}
			}
		default:
			fmt.Printf("FatalError %T!\n", v)
		}
	}
}
