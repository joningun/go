package matcher

import (
	"log"
	"time"

	"mdfinder.com/matcher/matcher/simg"

	"github.com/wangjia184/sortedset"

	"mdfinder.com/matcher/matcher/glayer"
)

type GG struct {
	graphDB         *simg.TSGraph
	groups          *glayer.GraphDataLayer
	linkGroupSample *glayer.GraphDataArrow //From: Group, To: Sample
	samples         *glayer.GraphDataLayer
	linkSampleTags  *glayer.GraphDataArrow //From: Group, To: Sample
	tags            *glayer.GraphDataLayer
	chronicGroups   *sortedset.SortedSet
}

func NewGG() *GG {
	g := simg.New()
	groups := glayer.New(g)
	samples := glayer.New(g)
	tags := glayer.New(g)
	return &GG{
		graphDB:         g,
		groups:          groups,
		linkGroupSample: glayer.NewArrow(groups, samples),
		samples:         samples,
		linkSampleTags:  glayer.NewArrow(samples, tags),
		tags:            tags,
		chronicGroups:   sortedset.New(),
	}
}
func (gg *GG) GroupOf(id string) Group {
	return gg.groups.ValueOfID(id).(Group)
}
func (gg *GG) AddSamples(samples []Sample) {
	for _, sam := range samples {
		gg.samples.Add(sam)
		for _, feat := range sam.Features {
			gg.tags.Add(Tag(feat.Key))
			gg.linkSampleTags.Link(sam.ID(), feat.Key)
		}
	}
}
func (gg *GG) SampleOf(id string) *Sample {
	if sh := gg.samples.ValueOfID(id); sh != nil {
		sam := sh.(Sample)
		return &sam
	}
	return nil
}
func (gg *GG) GetGroupOfSampleID(sampleID string) *Group {
	groups := gg.linkGroupSample.ObjsTo(sampleID)
	if groups == nil {
		return nil
	}
	for _, g := range groups {
		group := g.(Group)
		return &group
	}
	return nil
}

func (gg *GG) GetNumberOfSamplesInGroup(g string) int {
	if sams := gg.linkGroupSample.ObjsFrom(g); sams != nil {
		return len(sams)
	} else {
		panic("Group doesnt exists. " + g)
	}
}

//go:generate goderive .
//GetSamplesOfGroup fuck
func (gg *GG) GetSamplesOfGroup(groupID string) []Sample {
	if sams := gg.linkGroupSample.ObjsFrom(groupID); sams != nil {
		fm := func(sam glayer.IDable) Sample {
			return sam.(Sample)
		}
		return deriveFmap_IDable_Sample(fm, sams)
	} else {
		panic("group doesnt exists: " + groupID)
	}
}

func (gg *GG) JoinSampleToGroup(s string, g string) {
	gg.linkGroupSample.Link(g, s)
}

func (gg *GG) AddGroup(g Group) {
	gg.groups.Add(g)
	score := sortedset.SCORE(time.Now().Unix())
	gg.chronicGroups.AddOrUpdate(g.id, score, g)
}

func (gg *GG) RemoveSample(id string) {
	val := gg.samples.ValueOfID(id)
	if val == nil {
		return
	}
	sam := val.(Sample)
	pG := gg.GetGroupOfSampleID(sam.Id)
	gg.samples.Rm(id)
	if pG != nil {
		if gg.GetNumberOfSamplesInGroup(pG.id) == 0 {
			log.Printf("last group memeber left. Removing group %s", pG.ID())
			gg.groups.Rm(pG.ID())
			gg.chronicGroups.Remove(pG.id)
		}
	}
}

func (gg *GG) PagedChronicGroup(offset, count int) []Group {
	chronic := gg.chronicGroups
	nodeMap := func(node *sortedset.SortedSetNode) Group {
		return node.Value.(Group)
	}
	nodes := chronic.GetByRankRange(offset, offset+count, false)
	return deriveFmap_Node_Group(nodeMap, nodes)
}

func (gg *GG) LonelySamples() []Sample {
	fm := func(o glayer.IDable) Sample {
		return o.(Sample)
	}
	return deriveFmap_IDable_Sample(fm, gg.linkGroupSample.LonelyToObjs())
}

func (gg *GG) NumberOfSamplesForTag(tag string) int {
	return len(gg.linkSampleTags.ObjsTo(tag))
}
