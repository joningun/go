package matcher

import (
	"fmt"
	"log"
	"testing"
	"time"
)

func NwD(id string, f1 string, p1 int64) Sample {
	return NewSample(id, []Feature{
		NewFeature(f1, p1),
	})
}
func NwDD(id string, f1 string, p1 int64, f2 string, p2 int64) Sample {
	return NewSample(id, []Feature{
		NewFeature(f1, p1),
		NewFeature(f2, p2),
	})
}

func TestFuck(t *testing.T) {
	matcher := New(2)

	matcher.Start()
	printGroups(matcher, 5, 5)
	matcher.QueueSample(NwD("a", "a", 10))
	matcher.QueueSample(NwD("b", "a", 10))
	matcher.QueueSample(NwD("c", "a", 10))
	time.Sleep(3 * time.Second)
	matcher.QueueSample(NwD("d", "a", 10))
	matcher.QueueSample(NwD("e", "a", 10))
	matcher.QueueSample(NwD("f", "b", 10))
	time.Sleep(3 * time.Second)
	matcher.QueueSample(NwD("g", "b", 10))
	matcher.QueueSample(NwD("h", "b", 10))
	matcher.QueueSample(NwD("i", "b", 10))
	time.Sleep(3 * time.Second)
	matcher.QueueSample(NwD("j", "a", 10))
	matcher.QueueSample(NwD("k", "c", 10))
	matcher.QueueSample(NwD("l", "c", 10))

	groupOfI := matcher.DB.GetGroupOfSampleID("i")
	matcher.DirectMatch(NewSample("Joker", []Feature{}), groupOfI.id)

	time.Sleep(3 * time.Second)

	printGroups(matcher, 0, 5)
	time.Sleep(3 * time.Second)

	log.Println("---unmatch test start---")

	matcher.QueueSample(NwD("U1", "c", 10))
	time.Sleep(time.Second / 10)
	matcher.Unmatch("U1")
	time.Sleep(time.Second / 10)
	matcher.Unmatch("a")
	time.Sleep(time.Second)
	printGroups(matcher, 0, 5)
}

func printGroups(matcher *Matcher, offset, limit int) {
	groups := matcher.DB.PagedChronicGroup(offset, limit)
	fmt.Println("groups: ")
	for i, g := range groups {
		samples := matcher.DB.GetSamplesOfGroup(g.id)
		log.Printf("%d. %s %s,", i, g.CreatedTime().Format(time.RFC3339), g.id)
		for _, s := range samples {
			log.Printf("  %s: %v", s.Id, s.Features)
		}
	}
}
