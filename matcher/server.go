package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"mdfinder.com/matcher/matcher"
	"mdfinder.com/matcher/mp3"
)

type rpc struct {
	worker *matcher.Matcher
	redi   *Redi
}

func (r *rpc) Ping(ctx context.Context, _ *mp3.VoidMsg) (*mp3.VoidMsg, error) {
	return &mp3.VoidMsg{}, nil
}
func featMapIn(f *mp3.Feature) matcher.Feature {
	return matcher.NewFeature(f.Key, f.Plus)
}
func sampleMapIn(s *mp3.Sample) matcher.Sample {
	return matcher.NewSample(s.Id, deriveFmap(featMapIn, s.Features))
}

//go:generate goderive .
func (r *rpc) QueueForMatch(ctx context.Context, sample *mp3.Sample) (*mp3.VoidMsg, error) {
	if r.worker.DB.SampleOf(sample.Id) != nil {
		fmt.Printf("Rejected already existing sample . %s", sample.Id)
		return nil, fmt.Errorf("sample already exists")
	}
	fmap := func(a *mp3.Feature) string {
		return a.Key
	}
	go r.redi.TagsIn(deriveFmapPBFeatTag(fmap, sample.Features))
	log.Printf("new sample arrived: %v", sample)

	go r.worker.QueueSample(sampleMapIn(sample))
	return &mp3.VoidMsg{}, nil
}

func (r *rpc) retireTagsOfSample(sampleID string) {
	if sampleForTag := r.worker.DB.SampleOf(sampleID); sampleForTag != nil {
		sampleCopy := *sampleForTag
		fmap := func(a matcher.Feature) string {
			return a.Key
		}
		tags := deriveFmapFeatTag(fmap, sampleCopy.Features)
		r.redi.TagsOut(tags)
	}
}
func (r *rpc) Unmatch(ctx context.Context, sID *mp3.StringMsg) (void *mp3.VoidMsg, err error) {
	go r.retireTagsOfSample(sID.Str)
	go r.worker.Unmatch(sID.Str)
	return &mp3.VoidMsg{}, nil
}
func featMapOut(a matcher.Feature) *mp3.Feature {
	return &mp3.Feature{
		Key:  a.Key,
		Plus: a.Plus,
	}
}
func sampleMapOut(a matcher.Sample) *mp3.Sample {
	return &mp3.Sample{
		Id:       a.ID(),
		Features: deriveFmapFeatOut(featMapOut, a.Features),
	}
}
func (r *rpc) GetSampleInfo(ctx context.Context, sID *mp3.StringMsg) (info *mp3.InfoMsg, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Println("getsample panic: %v", e)
			err = fmt.Errorf("GetSampleInfo paniced: %v", e)
		}
	}()
	sampleID := sID.Str

	var groupInfo *mp3.GroupInfo

	if group := r.worker.DB.GetGroupOfSampleID(sampleID); group != nil {
		samples := r.worker.DB.GetSamplesOfGroup(group.ID())
		groupInfo = &mp3.GroupInfo{
			Id:      group.ID(),
			Samples: deriveFmapSampleOut(sampleMapOut, samples),
		}
	}

	var mySampleInfo *mp3.Sample

	if mysample := r.worker.DB.SampleOf(sampleID); mysample != nil {
		mySampleInfo = sampleMapOut(*mysample)
	}

	info = &mp3.InfoMsg{
		Group:    groupInfo,
		Mysample: mySampleInfo,
	}
	return
}

func (r *rpc) HotTags(context.Context, *mp3.VoidMsg) (*mp3.HotTagsMsg, error) {
	tags := r.redi.HotTags()
	fm := func(tag string) *mp3.HotTag {

		return &mp3.HotTag{
			Tag:       tag,
			SampleCnt: int32(r.worker.DB.NumberOfSamplesForTag(tag)),
		}
	}
	return &mp3.HotTagsMsg{
		Tags: deriveFmap_Tag_HotTag(fm, tags),
	}, nil
}
func (r *rpc) mapGroupsOut(groups []matcher.Group) []*mp3.GroupInfo {
	fmap := func(group matcher.Group) *mp3.GroupInfo {
		samples := deriveFmapSampleOut(sampleMapOut, r.worker.DB.GetSamplesOfGroup(group.ID()))
		return &mp3.GroupInfo{
			Id:          group.ID(),
			Samples:     samples,
			CreatedTime: group.CreatedTime().Format(time.RFC3339),
		}
	}
	return deriveFmapGroupOut(fmap, groups)
}
func (r *rpc) PagedGroups(ctx context.Context, pageinfo *mp3.PageInfo) (*mp3.Groups, error) {
	groups := r.worker.DB.PagedChronicGroup(int(pageinfo.Offset), int(pageinfo.Limit))
	return &mp3.Groups{
		Groups: r.mapGroupsOut(groups),
	}, nil
}

func (r *rpc) DirectMatch(ctx context.Context, direct *mp3.DirectMsg) (voidmsg *mp3.VoidMsg, e error) {
	go r.worker.DirectMatch(sampleMapIn(direct.Sample), direct.GroupID)
	return &mp3.VoidMsg{}, nil
}

func (r *rpc) GetGroupsInfo(_ context.Context, msg *mp3.StringsMsg) (*mp3.Groups, error) {
	fmap := func(id string) matcher.Group {
		return r.worker.DB.GroupOf(id)
	}
	groups := deriveFmapID2Group(fmap, msg.Ids)
	return &mp3.Groups{
		Groups: r.mapGroupsOut(groups),
	}, nil
}

const cRediAddr string = "redis:6379"

func main() {
	rediAddr := flag.String("redis", cRediAddr, "redi address <h>:<p>")
	flag.Parse()
	// serverAddr := "server"
	worker := matcher.New(5)
	worker.Start()

	lis, err := net.Listen("tcp", ":50030")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	//creds, _ := credentials.NewServerTLSFromFile("/fullchain1.pem", "/privkey1.pem")
	//s := grpc.NewServer(grpc.Creds(creds))
	s := grpc.NewServer()
	mp3.RegisterMatchingServer(s, &rpc{worker: worker, redi: NewRedi(*rediAddr)})
	// Register reflection service on gRPC server.
	reflection.Register(s)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
