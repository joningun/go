// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto.proto

package mp3

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type VoidMsg struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *VoidMsg) Reset()         { *m = VoidMsg{} }
func (m *VoidMsg) String() string { return proto.CompactTextString(m) }
func (*VoidMsg) ProtoMessage()    {}
func (*VoidMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{0}
}

func (m *VoidMsg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_VoidMsg.Unmarshal(m, b)
}
func (m *VoidMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_VoidMsg.Marshal(b, m, deterministic)
}
func (m *VoidMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_VoidMsg.Merge(m, src)
}
func (m *VoidMsg) XXX_Size() int {
	return xxx_messageInfo_VoidMsg.Size(m)
}
func (m *VoidMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_VoidMsg.DiscardUnknown(m)
}

var xxx_messageInfo_VoidMsg proto.InternalMessageInfo

type StringMsg struct {
	Str                  string   `protobuf:"bytes,1,opt,name=str,proto3" json:"str,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StringMsg) Reset()         { *m = StringMsg{} }
func (m *StringMsg) String() string { return proto.CompactTextString(m) }
func (*StringMsg) ProtoMessage()    {}
func (*StringMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{1}
}

func (m *StringMsg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StringMsg.Unmarshal(m, b)
}
func (m *StringMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StringMsg.Marshal(b, m, deterministic)
}
func (m *StringMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StringMsg.Merge(m, src)
}
func (m *StringMsg) XXX_Size() int {
	return xxx_messageInfo_StringMsg.Size(m)
}
func (m *StringMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_StringMsg.DiscardUnknown(m)
}

var xxx_messageInfo_StringMsg proto.InternalMessageInfo

func (m *StringMsg) GetStr() string {
	if m != nil {
		return m.Str
	}
	return ""
}

type StringsMsg struct {
	Ids                  []string `protobuf:"bytes,1,rep,name=ids,proto3" json:"ids,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StringsMsg) Reset()         { *m = StringsMsg{} }
func (m *StringsMsg) String() string { return proto.CompactTextString(m) }
func (*StringsMsg) ProtoMessage()    {}
func (*StringsMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{2}
}

func (m *StringsMsg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StringsMsg.Unmarshal(m, b)
}
func (m *StringsMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StringsMsg.Marshal(b, m, deterministic)
}
func (m *StringsMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StringsMsg.Merge(m, src)
}
func (m *StringsMsg) XXX_Size() int {
	return xxx_messageInfo_StringsMsg.Size(m)
}
func (m *StringsMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_StringsMsg.DiscardUnknown(m)
}

var xxx_messageInfo_StringsMsg proto.InternalMessageInfo

func (m *StringsMsg) GetIds() []string {
	if m != nil {
		return m.Ids
	}
	return nil
}

type Sample struct {
	Id                   string     `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Features             []*Feature `protobuf:"bytes,2,rep,name=features,proto3" json:"features,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *Sample) Reset()         { *m = Sample{} }
func (m *Sample) String() string { return proto.CompactTextString(m) }
func (*Sample) ProtoMessage()    {}
func (*Sample) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{3}
}

func (m *Sample) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Sample.Unmarshal(m, b)
}
func (m *Sample) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Sample.Marshal(b, m, deterministic)
}
func (m *Sample) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Sample.Merge(m, src)
}
func (m *Sample) XXX_Size() int {
	return xxx_messageInfo_Sample.Size(m)
}
func (m *Sample) XXX_DiscardUnknown() {
	xxx_messageInfo_Sample.DiscardUnknown(m)
}

var xxx_messageInfo_Sample proto.InternalMessageInfo

func (m *Sample) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Sample) GetFeatures() []*Feature {
	if m != nil {
		return m.Features
	}
	return nil
}

type Feature struct {
	Key                  string   `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
	Plus                 int64    `protobuf:"varint,2,opt,name=plus,proto3" json:"plus,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Feature) Reset()         { *m = Feature{} }
func (m *Feature) String() string { return proto.CompactTextString(m) }
func (*Feature) ProtoMessage()    {}
func (*Feature) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{4}
}

func (m *Feature) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Feature.Unmarshal(m, b)
}
func (m *Feature) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Feature.Marshal(b, m, deterministic)
}
func (m *Feature) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Feature.Merge(m, src)
}
func (m *Feature) XXX_Size() int {
	return xxx_messageInfo_Feature.Size(m)
}
func (m *Feature) XXX_DiscardUnknown() {
	xxx_messageInfo_Feature.DiscardUnknown(m)
}

var xxx_messageInfo_Feature proto.InternalMessageInfo

func (m *Feature) GetKey() string {
	if m != nil {
		return m.Key
	}
	return ""
}

func (m *Feature) GetPlus() int64 {
	if m != nil {
		return m.Plus
	}
	return 0
}

type InfoMsg struct {
	Group                *GroupInfo `protobuf:"bytes,1,opt,name=group,proto3" json:"group,omitempty"`
	Mysample             *Sample    `protobuf:"bytes,2,opt,name=mysample,proto3" json:"mysample,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *InfoMsg) Reset()         { *m = InfoMsg{} }
func (m *InfoMsg) String() string { return proto.CompactTextString(m) }
func (*InfoMsg) ProtoMessage()    {}
func (*InfoMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{5}
}

func (m *InfoMsg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_InfoMsg.Unmarshal(m, b)
}
func (m *InfoMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_InfoMsg.Marshal(b, m, deterministic)
}
func (m *InfoMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_InfoMsg.Merge(m, src)
}
func (m *InfoMsg) XXX_Size() int {
	return xxx_messageInfo_InfoMsg.Size(m)
}
func (m *InfoMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_InfoMsg.DiscardUnknown(m)
}

var xxx_messageInfo_InfoMsg proto.InternalMessageInfo

func (m *InfoMsg) GetGroup() *GroupInfo {
	if m != nil {
		return m.Group
	}
	return nil
}

func (m *InfoMsg) GetMysample() *Sample {
	if m != nil {
		return m.Mysample
	}
	return nil
}

type GroupInfo struct {
	Id                   string    `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Samples              []*Sample `protobuf:"bytes,2,rep,name=samples,proto3" json:"samples,omitempty"`
	CreatedTime          string    `protobuf:"bytes,3,opt,name=createdTime,proto3" json:"createdTime,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *GroupInfo) Reset()         { *m = GroupInfo{} }
func (m *GroupInfo) String() string { return proto.CompactTextString(m) }
func (*GroupInfo) ProtoMessage()    {}
func (*GroupInfo) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{6}
}

func (m *GroupInfo) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GroupInfo.Unmarshal(m, b)
}
func (m *GroupInfo) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GroupInfo.Marshal(b, m, deterministic)
}
func (m *GroupInfo) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GroupInfo.Merge(m, src)
}
func (m *GroupInfo) XXX_Size() int {
	return xxx_messageInfo_GroupInfo.Size(m)
}
func (m *GroupInfo) XXX_DiscardUnknown() {
	xxx_messageInfo_GroupInfo.DiscardUnknown(m)
}

var xxx_messageInfo_GroupInfo proto.InternalMessageInfo

func (m *GroupInfo) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *GroupInfo) GetSamples() []*Sample {
	if m != nil {
		return m.Samples
	}
	return nil
}

func (m *GroupInfo) GetCreatedTime() string {
	if m != nil {
		return m.CreatedTime
	}
	return ""
}

type PageInfo struct {
	Offset               int32    `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit                int32    `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PageInfo) Reset()         { *m = PageInfo{} }
func (m *PageInfo) String() string { return proto.CompactTextString(m) }
func (*PageInfo) ProtoMessage()    {}
func (*PageInfo) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{7}
}

func (m *PageInfo) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PageInfo.Unmarshal(m, b)
}
func (m *PageInfo) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PageInfo.Marshal(b, m, deterministic)
}
func (m *PageInfo) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PageInfo.Merge(m, src)
}
func (m *PageInfo) XXX_Size() int {
	return xxx_messageInfo_PageInfo.Size(m)
}
func (m *PageInfo) XXX_DiscardUnknown() {
	xxx_messageInfo_PageInfo.DiscardUnknown(m)
}

var xxx_messageInfo_PageInfo proto.InternalMessageInfo

func (m *PageInfo) GetOffset() int32 {
	if m != nil {
		return m.Offset
	}
	return 0
}

func (m *PageInfo) GetLimit() int32 {
	if m != nil {
		return m.Limit
	}
	return 0
}

type Groups struct {
	Groups               []*GroupInfo `protobuf:"bytes,1,rep,name=groups,proto3" json:"groups,omitempty"`
	XXX_NoUnkeyedLiteral struct{}     `json:"-"`
	XXX_unrecognized     []byte       `json:"-"`
	XXX_sizecache        int32        `json:"-"`
}

func (m *Groups) Reset()         { *m = Groups{} }
func (m *Groups) String() string { return proto.CompactTextString(m) }
func (*Groups) ProtoMessage()    {}
func (*Groups) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{8}
}

func (m *Groups) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Groups.Unmarshal(m, b)
}
func (m *Groups) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Groups.Marshal(b, m, deterministic)
}
func (m *Groups) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Groups.Merge(m, src)
}
func (m *Groups) XXX_Size() int {
	return xxx_messageInfo_Groups.Size(m)
}
func (m *Groups) XXX_DiscardUnknown() {
	xxx_messageInfo_Groups.DiscardUnknown(m)
}

var xxx_messageInfo_Groups proto.InternalMessageInfo

func (m *Groups) GetGroups() []*GroupInfo {
	if m != nil {
		return m.Groups
	}
	return nil
}

type DirectMsg struct {
	Sample               *Sample  `protobuf:"bytes,1,opt,name=sample,proto3" json:"sample,omitempty"`
	GroupID              string   `protobuf:"bytes,2,opt,name=groupID,proto3" json:"groupID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DirectMsg) Reset()         { *m = DirectMsg{} }
func (m *DirectMsg) String() string { return proto.CompactTextString(m) }
func (*DirectMsg) ProtoMessage()    {}
func (*DirectMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{9}
}

func (m *DirectMsg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DirectMsg.Unmarshal(m, b)
}
func (m *DirectMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DirectMsg.Marshal(b, m, deterministic)
}
func (m *DirectMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DirectMsg.Merge(m, src)
}
func (m *DirectMsg) XXX_Size() int {
	return xxx_messageInfo_DirectMsg.Size(m)
}
func (m *DirectMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_DirectMsg.DiscardUnknown(m)
}

var xxx_messageInfo_DirectMsg proto.InternalMessageInfo

func (m *DirectMsg) GetSample() *Sample {
	if m != nil {
		return m.Sample
	}
	return nil
}

func (m *DirectMsg) GetGroupID() string {
	if m != nil {
		return m.GroupID
	}
	return ""
}

type HotTag struct {
	Tag                  string   `protobuf:"bytes,1,opt,name=tag,proto3" json:"tag,omitempty"`
	SampleCnt            int32    `protobuf:"varint,2,opt,name=sampleCnt,proto3" json:"sampleCnt,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HotTag) Reset()         { *m = HotTag{} }
func (m *HotTag) String() string { return proto.CompactTextString(m) }
func (*HotTag) ProtoMessage()    {}
func (*HotTag) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{10}
}

func (m *HotTag) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HotTag.Unmarshal(m, b)
}
func (m *HotTag) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HotTag.Marshal(b, m, deterministic)
}
func (m *HotTag) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HotTag.Merge(m, src)
}
func (m *HotTag) XXX_Size() int {
	return xxx_messageInfo_HotTag.Size(m)
}
func (m *HotTag) XXX_DiscardUnknown() {
	xxx_messageInfo_HotTag.DiscardUnknown(m)
}

var xxx_messageInfo_HotTag proto.InternalMessageInfo

func (m *HotTag) GetTag() string {
	if m != nil {
		return m.Tag
	}
	return ""
}

func (m *HotTag) GetSampleCnt() int32 {
	if m != nil {
		return m.SampleCnt
	}
	return 0
}

type HotTagsMsg struct {
	Tags                 []*HotTag `protobuf:"bytes,1,rep,name=tags,proto3" json:"tags,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *HotTagsMsg) Reset()         { *m = HotTagsMsg{} }
func (m *HotTagsMsg) String() string { return proto.CompactTextString(m) }
func (*HotTagsMsg) ProtoMessage()    {}
func (*HotTagsMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{11}
}

func (m *HotTagsMsg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HotTagsMsg.Unmarshal(m, b)
}
func (m *HotTagsMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HotTagsMsg.Marshal(b, m, deterministic)
}
func (m *HotTagsMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HotTagsMsg.Merge(m, src)
}
func (m *HotTagsMsg) XXX_Size() int {
	return xxx_messageInfo_HotTagsMsg.Size(m)
}
func (m *HotTagsMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_HotTagsMsg.DiscardUnknown(m)
}

var xxx_messageInfo_HotTagsMsg proto.InternalMessageInfo

func (m *HotTagsMsg) GetTags() []*HotTag {
	if m != nil {
		return m.Tags
	}
	return nil
}

func init() {
	proto.RegisterType((*VoidMsg)(nil), "mp3.VoidMsg")
	proto.RegisterType((*StringMsg)(nil), "mp3.StringMsg")
	proto.RegisterType((*StringsMsg)(nil), "mp3.StringsMsg")
	proto.RegisterType((*Sample)(nil), "mp3.Sample")
	proto.RegisterType((*Feature)(nil), "mp3.Feature")
	proto.RegisterType((*InfoMsg)(nil), "mp3.InfoMsg")
	proto.RegisterType((*GroupInfo)(nil), "mp3.GroupInfo")
	proto.RegisterType((*PageInfo)(nil), "mp3.PageInfo")
	proto.RegisterType((*Groups)(nil), "mp3.Groups")
	proto.RegisterType((*DirectMsg)(nil), "mp3.DirectMsg")
	proto.RegisterType((*HotTag)(nil), "mp3.HotTag")
	proto.RegisterType((*HotTagsMsg)(nil), "mp3.HotTagsMsg")
}

func init() { proto.RegisterFile("proto.proto", fileDescriptor_2fcc84b9998d60d8) }

var fileDescriptor_2fcc84b9998d60d8 = []byte{
	// 553 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x74, 0x54, 0xdb, 0x6e, 0xda, 0x40,
	0x10, 0xc5, 0x18, 0x6c, 0x3c, 0x2e, 0x49, 0xb5, 0xaa, 0x5a, 0x14, 0xf5, 0x82, 0xb6, 0x37, 0x7a,
	0x89, 0x53, 0xc1, 0x4b, 0x9e, 0x69, 0x94, 0x34, 0x95, 0x90, 0xa8, 0x49, 0xab, 0xbe, 0xba, 0x78,
	0x71, 0x57, 0xc1, 0x17, 0xed, 0x2e, 0x95, 0xf8, 0xee, 0xfe, 0x40, 0xb5, 0xb3, 0x6b, 0x42, 0xe2,
	0xe6, 0x05, 0x7b, 0xe7, 0xcc, 0x99, 0x39, 0xb3, 0x73, 0x30, 0x84, 0x95, 0x28, 0x55, 0x19, 0xe1,
	0x2f, 0x71, 0xf3, 0x6a, 0x42, 0x03, 0xf0, 0x7f, 0x94, 0x3c, 0x9d, 0xc9, 0x8c, 0x3e, 0x83, 0x60,
	0xa1, 0x04, 0x2f, 0xb2, 0x99, 0xcc, 0xc8, 0x43, 0x70, 0xa5, 0x12, 0x03, 0x67, 0xe8, 0x8c, 0x82,
	0x58, 0xbf, 0xd2, 0xe7, 0x00, 0x06, 0x96, 0x16, 0xe7, 0xa9, 0x1c, 0x38, 0x43, 0x57, 0xe3, 0x3c,
	0x95, 0x74, 0x0a, 0xde, 0x22, 0xc9, 0xab, 0x35, 0x23, 0x07, 0xd0, 0xe6, 0xa9, 0xa5, 0xb6, 0x79,
	0x4a, 0x46, 0xd0, 0x5b, 0xb1, 0x44, 0x6d, 0x04, 0x93, 0x83, 0xf6, 0xd0, 0x1d, 0x85, 0xe3, 0x07,
	0x51, 0x5e, 0x4d, 0xa2, 0x73, 0x13, 0x8c, 0x77, 0x28, 0x3d, 0x01, 0xdf, 0x06, 0x75, 0x83, 0x6b,
	0xb6, 0xad, 0x05, 0x5c, 0xb3, 0x2d, 0x21, 0xd0, 0xa9, 0xd6, 0x1b, 0x5d, 0xc2, 0x19, 0xb9, 0x31,
	0xbe, 0xd3, 0x9f, 0xe0, 0x5f, 0x16, 0xab, 0x52, 0x2b, 0x7a, 0x05, 0xdd, 0x4c, 0x94, 0x9b, 0x0a,
	0x29, 0xe1, 0xf8, 0x00, 0x5b, 0x5c, 0xe8, 0x88, 0xce, 0x88, 0x0d, 0x48, 0xde, 0x42, 0x2f, 0xdf,
	0x4a, 0xd4, 0x89, 0x85, 0xc2, 0x71, 0x88, 0x89, 0x46, 0x7a, 0xbc, 0x03, 0x69, 0x0a, 0xc1, 0x8e,
	0xdc, 0x98, 0xe8, 0x35, 0xf8, 0x26, 0xad, 0x1e, 0xe8, 0x56, 0x91, 0x1a, 0x23, 0x43, 0x08, 0x97,
	0x82, 0x25, 0x8a, 0xa5, 0x57, 0x3c, 0x67, 0x03, 0x17, 0xf9, 0xfb, 0x21, 0x7a, 0x0a, 0xbd, 0x79,
	0x92, 0x31, 0x6c, 0xf2, 0x18, 0xbc, 0x72, 0xb5, 0x92, 0x4c, 0x61, 0xa3, 0x6e, 0x6c, 0x4f, 0xe4,
	0x11, 0x74, 0xd7, 0x3c, 0xe7, 0x0a, 0xf5, 0x76, 0x63, 0x73, 0xa0, 0x9f, 0xc0, 0x43, 0x7d, 0x92,
	0xbc, 0x01, 0x0f, 0x67, 0x33, 0xdb, 0x68, 0x4e, 0x6e, 0x51, 0xfa, 0x15, 0x82, 0x33, 0x2e, 0xd8,
	0x52, 0xe9, 0xdb, 0x7a, 0x09, 0x9e, 0xbd, 0x05, 0xa7, 0x79, 0x0b, 0x16, 0x22, 0x03, 0xf0, 0x91,
	0x7b, 0x79, 0x86, 0xbd, 0x83, 0xb8, 0x3e, 0xd2, 0x53, 0xf0, 0xbe, 0x94, 0xea, 0x2a, 0x41, 0x23,
	0xa8, 0x24, 0xab, 0xf7, 0xa4, 0x92, 0x8c, 0x3c, 0x85, 0xc0, 0xf0, 0x3f, 0x17, 0xb5, 0xe6, 0x9b,
	0x00, 0x3d, 0x06, 0x30, 0x4c, 0xb4, 0xd1, 0x0b, 0xe8, 0xa8, 0x24, 0xab, 0x95, 0x1b, 0x11, 0x06,
	0x8e, 0x11, 0x18, 0xff, 0x6d, 0x43, 0x6f, 0x96, 0xa8, 0xe5, 0x6f, 0x5e, 0xe8, 0x15, 0x77, 0xe6,
	0xfa, 0x69, 0xec, 0x63, 0x7d, 0x7b, 0x74, 0xeb, 0x44, 0x5b, 0xe4, 0x23, 0xf4, 0xbf, 0x6d, 0xd8,
	0x86, 0x9d, 0x97, 0x02, 0x99, 0x64, 0x7f, 0xb6, 0x46, 0xf6, 0x3b, 0xf0, 0xbf, 0x17, 0x39, 0xe6,
	0x99, 0x8b, 0xdb, 0xfd, 0x07, 0x1a, 0xa9, 0x27, 0xd0, 0xbf, 0x60, 0xca, 0xd4, 0x31, 0xb6, 0xf8,
	0x2f, 0xc1, 0x1a, 0x92, 0xb6, 0xc8, 0x7b, 0xf0, 0xed, 0xac, 0x77, 0x24, 0x1f, 0xee, 0x0d, 0x2a,
	0x4d, 0xee, 0x07, 0x08, 0xb5, 0x13, 0x52, 0xbb, 0xd4, 0x3e, 0x66, 0xd4, 0xde, 0x38, 0x0a, 0x6f,
	0x76, 0x2a, 0x69, 0x8b, 0x1c, 0x43, 0x68, 0x57, 0xb9, 0x27, 0x7c, 0xb7, 0xdc, 0x7b, 0x84, 0x1b,
	0x36, 0x0a, 0x3f, 0xdc, 0x13, 0xae, 0xfb, 0xdf, 0xa9, 0x3f, 0x1d, 0xc1, 0x13, 0x5e, 0x46, 0x99,
	0xa8, 0x96, 0x51, 0x9e, 0xae, 0x78, 0x91, 0x32, 0x11, 0x49, 0x26, 0xfe, 0x30, 0x31, 0x0d, 0x17,
	0xf8, 0x9c, 0xeb, 0x4f, 0xc8, 0xdc, 0xf9, 0xe5, 0xe1, 0xb7, 0x64, 0xf2, 0x2f, 0x00, 0x00, 0xff,
	0xff, 0x06, 0xef, 0xb8, 0xe9, 0x5a, 0x04, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// MatchingClient is the client API for Matching service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type MatchingClient interface {
	// Sends a greeting
	Ping(ctx context.Context, in *VoidMsg, opts ...grpc.CallOption) (*VoidMsg, error)
	QueueForMatch(ctx context.Context, in *Sample, opts ...grpc.CallOption) (*VoidMsg, error)
	Unmatch(ctx context.Context, in *StringMsg, opts ...grpc.CallOption) (*VoidMsg, error)
	GetSampleInfo(ctx context.Context, in *StringMsg, opts ...grpc.CallOption) (*InfoMsg, error)
	HotTags(ctx context.Context, in *VoidMsg, opts ...grpc.CallOption) (*HotTagsMsg, error)
	PagedGroups(ctx context.Context, in *PageInfo, opts ...grpc.CallOption) (*Groups, error)
	DirectMatch(ctx context.Context, in *DirectMsg, opts ...grpc.CallOption) (*VoidMsg, error)
	GetGroupsInfo(ctx context.Context, in *StringsMsg, opts ...grpc.CallOption) (*Groups, error)
}

type matchingClient struct {
	cc *grpc.ClientConn
}

func NewMatchingClient(cc *grpc.ClientConn) MatchingClient {
	return &matchingClient{cc}
}

func (c *matchingClient) Ping(ctx context.Context, in *VoidMsg, opts ...grpc.CallOption) (*VoidMsg, error) {
	out := new(VoidMsg)
	err := c.cc.Invoke(ctx, "/mp3.Matching/Ping", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *matchingClient) QueueForMatch(ctx context.Context, in *Sample, opts ...grpc.CallOption) (*VoidMsg, error) {
	out := new(VoidMsg)
	err := c.cc.Invoke(ctx, "/mp3.Matching/QueueForMatch", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *matchingClient) Unmatch(ctx context.Context, in *StringMsg, opts ...grpc.CallOption) (*VoidMsg, error) {
	out := new(VoidMsg)
	err := c.cc.Invoke(ctx, "/mp3.Matching/Unmatch", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *matchingClient) GetSampleInfo(ctx context.Context, in *StringMsg, opts ...grpc.CallOption) (*InfoMsg, error) {
	out := new(InfoMsg)
	err := c.cc.Invoke(ctx, "/mp3.Matching/GetSampleInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *matchingClient) HotTags(ctx context.Context, in *VoidMsg, opts ...grpc.CallOption) (*HotTagsMsg, error) {
	out := new(HotTagsMsg)
	err := c.cc.Invoke(ctx, "/mp3.Matching/HotTags", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *matchingClient) PagedGroups(ctx context.Context, in *PageInfo, opts ...grpc.CallOption) (*Groups, error) {
	out := new(Groups)
	err := c.cc.Invoke(ctx, "/mp3.Matching/PagedGroups", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *matchingClient) DirectMatch(ctx context.Context, in *DirectMsg, opts ...grpc.CallOption) (*VoidMsg, error) {
	out := new(VoidMsg)
	err := c.cc.Invoke(ctx, "/mp3.Matching/DirectMatch", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *matchingClient) GetGroupsInfo(ctx context.Context, in *StringsMsg, opts ...grpc.CallOption) (*Groups, error) {
	out := new(Groups)
	err := c.cc.Invoke(ctx, "/mp3.Matching/GetGroupsInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MatchingServer is the server API for Matching service.
type MatchingServer interface {
	// Sends a greeting
	Ping(context.Context, *VoidMsg) (*VoidMsg, error)
	QueueForMatch(context.Context, *Sample) (*VoidMsg, error)
	Unmatch(context.Context, *StringMsg) (*VoidMsg, error)
	GetSampleInfo(context.Context, *StringMsg) (*InfoMsg, error)
	HotTags(context.Context, *VoidMsg) (*HotTagsMsg, error)
	PagedGroups(context.Context, *PageInfo) (*Groups, error)
	DirectMatch(context.Context, *DirectMsg) (*VoidMsg, error)
	GetGroupsInfo(context.Context, *StringsMsg) (*Groups, error)
}

func RegisterMatchingServer(s *grpc.Server, srv MatchingServer) {
	s.RegisterService(&_Matching_serviceDesc, srv)
}

func _Matching_Ping_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(VoidMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MatchingServer).Ping(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mp3.Matching/Ping",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MatchingServer).Ping(ctx, req.(*VoidMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _Matching_QueueForMatch_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Sample)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MatchingServer).QueueForMatch(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mp3.Matching/QueueForMatch",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MatchingServer).QueueForMatch(ctx, req.(*Sample))
	}
	return interceptor(ctx, in, info, handler)
}

func _Matching_Unmatch_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StringMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MatchingServer).Unmatch(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mp3.Matching/Unmatch",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MatchingServer).Unmatch(ctx, req.(*StringMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _Matching_GetSampleInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StringMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MatchingServer).GetSampleInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mp3.Matching/GetSampleInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MatchingServer).GetSampleInfo(ctx, req.(*StringMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _Matching_HotTags_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(VoidMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MatchingServer).HotTags(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mp3.Matching/HotTags",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MatchingServer).HotTags(ctx, req.(*VoidMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _Matching_PagedGroups_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PageInfo)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MatchingServer).PagedGroups(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mp3.Matching/PagedGroups",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MatchingServer).PagedGroups(ctx, req.(*PageInfo))
	}
	return interceptor(ctx, in, info, handler)
}

func _Matching_DirectMatch_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DirectMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MatchingServer).DirectMatch(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mp3.Matching/DirectMatch",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MatchingServer).DirectMatch(ctx, req.(*DirectMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _Matching_GetGroupsInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StringsMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MatchingServer).GetGroupsInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mp3.Matching/GetGroupsInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MatchingServer).GetGroupsInfo(ctx, req.(*StringsMsg))
	}
	return interceptor(ctx, in, info, handler)
}

var _Matching_serviceDesc = grpc.ServiceDesc{
	ServiceName: "mp3.Matching",
	HandlerType: (*MatchingServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Ping",
			Handler:    _Matching_Ping_Handler,
		},
		{
			MethodName: "QueueForMatch",
			Handler:    _Matching_QueueForMatch_Handler,
		},
		{
			MethodName: "Unmatch",
			Handler:    _Matching_Unmatch_Handler,
		},
		{
			MethodName: "GetSampleInfo",
			Handler:    _Matching_GetSampleInfo_Handler,
		},
		{
			MethodName: "HotTags",
			Handler:    _Matching_HotTags_Handler,
		},
		{
			MethodName: "PagedGroups",
			Handler:    _Matching_PagedGroups_Handler,
		},
		{
			MethodName: "DirectMatch",
			Handler:    _Matching_DirectMatch_Handler,
		},
		{
			MethodName: "GetGroupsInfo",
			Handler:    _Matching_GetGroupsInfo_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto.proto",
}
