package main

import (
	"fmt"

	"github.com/go-redis/redis"
)

type Redi struct {
	client *redis.Client
}

func NewRedi(addr string) *Redi {
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	pong, err := client.Ping().Result()
	fmt.Println("redis ping: ", pong, err)
	if err != nil {
		panic(err)
	}
	return &Redi{client: client}
}

const zsetKey = "matcher:tags"

func (r *Redi) TagsIn(tags []string) {
	pipeliner := r.client.Pipeline()
	for _, tag := range tags {
		pipeliner.ZIncrBy(zsetKey, 2, tag)
	}
	_, e := pipeliner.Exec()
	if e != nil {
		panic(e)
	}
}
func (r *Redi) TagsOut(tags []string) {
	pipeliner := r.client.Pipeline()
	for _, tag := range tags {
		pipeliner.ZIncrBy(zsetKey, -1, tag)
	}
	_, e := pipeliner.Exec()
	if e != nil {
		panic(e)
	}
}
func (r *Redi) HotTags() []string {
	tags, e := r.client.ZRevRange(zsetKey, 0, 50).Result()
	if e != nil {
		panic(e)
	}
	return tags
}
